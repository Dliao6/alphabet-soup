import sys

def main():
    # define command-line argument
    if len(sys.argv) != 2:
        sys.exit("Error: 2 arguments required.\nFormat: 'alphabet-soup.py FILENAME'")
    
    # read input, separate dimensions, letters, words
    file_name = sys.argv[1]
    with open(file_name, 'r') as f:
        file_contents = f.read().splitlines()
    dimensions = file_contents[0].split('x')
    end_of_grid = int(dimensions[0]) + 1
    grid = file_contents[1:end_of_grid]
    words = file_contents[end_of_grid:]

    # remove whitespace in puzzle grid, insert into 2D array
    letter_array = []
    for line in grid:
        char_list = line.split(' ')
        letter_array.append(char_list)

    # loop through list of words, for each word get end_coord and print
    for word in words:
        for i in range(len(letter_array)):
            for j in range(len(letter_array[i])):
                end_coord = get_end_coord(letter_array, word, i, j)
                if letter_array[i][j] == word[0] and end_coord != False:
                    print(word + " " + str(i) + ":" + str(j) + " " + end_coord)

# get_end_coord function, takes in word puzzle, word to search, starting row/col indices
def get_end_coord(board, word, row, col):
    for i in range(-1, 2):
        for j in range(-1, 2):
            # corner case: skip if i and j == 0. If both == 0, search does not progress
            if i == 0 and j == 0:
                continue
            # if a search has successfully completed, return the results
            search_result = search(board, word, row, col, i, j)
            if search_result != False:
                return search_result
    return False

# search function, takes in word puzzle, starting row/col indices, and two x-y variables from -1 to 1
def search(board, word, row, col, x_direction, y_direction):
    word_length = len(word)
    for i in range(word_length):
        # x_scale and y_scale determine whether i gets incremented up or down for search direction
        x_scale = i * x_direction
        y_scale = i * y_direction
        # next row/column values to evaluate
        next_row = row + y_scale
        next_col = col + x_scale
        # if word length is reached, return the last valid index
        if out_of_bounds(board, next_row, next_col) or board[next_row][next_col] != word[i]:
            # if proposed indices are invalid or don't match, break and return False
            break
        if i == word_length - 1:
            return str(next_row) + ":" + str(next_col)
        
    return False

# out_of_bounds function, takes in row and column indices and checks if valid
def out_of_bounds(board, row, col):
    if row < 0 or row >= len(board) or col < 0 or col >= len(board[row]):
        return True
    else:
        return False

if __name__ == "__main__":
    main()